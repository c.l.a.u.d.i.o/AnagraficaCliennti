<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Anagrafica;
use App\Http\Resources\Anagrafica as AnagraficaResource;

class AnagraficaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get Anagraficas
        $anagraficas = Anagrafica::paginate(100);

        //Return collection of Anagraficas as a resource
        return AnagraficaResource::collection($anagraficas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $anagrafica = Anagrafica::findOrFail($id);

        $anagrafica->id = $id;
        $anagrafica->name = $request->input('name');
        $anagrafica->address = $request->input('address');
        $anagrafica->city = $request->input('city');
        $anagrafica->cap = $request->input('cap');
        $anagrafica->piva = $request->input('piva');
        $anagrafica->phone = $request->input('phone');
        $anagrafica->note = $request->input('note');
        $anagrafica->email = $request->input('email');

        if($anagrafica->save()){
            return new AnagraficaResource($anagrafica);
        }
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $anagrafica =  new Anagrafica;

        $anagrafica->id = $request->input('id');
        $anagrafica->name = $request->input('name');
        $anagrafica->address = $request->input('address');
        $anagrafica->city = $request->input('city');
        $anagrafica->cap = $request->input('cap');
        $anagrafica->piva = $request->input('piva');
        $anagrafica->phone = $request->input('phone');
        $anagrafica->note = $request->input('note');
        $anagrafica->email = $request->input('email');

        if($anagrafica->save()){
            return new AnagraficaResource($anagrafica);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get anagrafica
        $anagrafica = Anagrafica::findOrFail($id);

        //Return single anagrafica as a resource
        return new AnagraficaResource($anagrafica);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                // Get anagrafica
                $anagrafica = Anagrafica::findOrFail($id);
                
                if($anagrafica->delete()){
                    return new AnagraficaResource($anagrafica);
                }
    }
}
