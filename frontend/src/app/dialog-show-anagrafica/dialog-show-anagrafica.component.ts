import { Component, OnInit, Inject } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AnagraficaService } from '../services/anagrafica.service';
@Component({
  selector: 'app-dialog-show-anagrafica',
  templateUrl: './dialog-show-anagrafica.component.html',
  styleUrls: ['./dialog-show-anagrafica.component.css']
})
export class DialogShowAnagraficaComponent implements OnInit {

  personId: Number;
  person: any = {
    name: "",
    address: "",
    city: "",
    cap: "",
    piva: "",
    phone: "",
    note: "",
    email: ""
  };

  constructor(
    private AnagraficaService: AnagraficaService,
    public dialogRef: MatDialogRef<DialogShowAnagraficaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.personId = data.id;
  }

  async ngOnInit() {
    this.person = await this.AnagraficaService.getAnagrafica(this.personId);
  }

  close(){
    this.dialogRef.close();
  }

}
