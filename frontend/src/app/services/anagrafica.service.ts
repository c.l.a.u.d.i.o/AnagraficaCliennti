import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class AnagraficaService {

  private anagraficaURL = 'https://fateltest.appspot.com/api/';

  constructor(private http: Http) { }

  async getComuni(): Promise<number> {
    const response = await this.http.get("./assets/comuni.json").toPromise();
    console.debug(response.json().data)
    return response.json().data;
  }

  async getAnagraficas(): Promise<number> {
    const response = await this.http.get(this.anagraficaURL+"anagraficas").toPromise();
    console.debug(response.json().data)
    return response.json().data;
  }

  async getAnagrafica(id:Number): Promise<number> {
    const response = await this.http.get(this.anagraficaURL+"anagrafica/" + id).toPromise();
    console.debug(response.json().data)
    return response.json().data;
  }

  async createAnagrafica(person:any): Promise<number> {
    const response = await this.http.post(this.anagraficaURL+"anagrafica", {
        name: person.name,
        address: person.address,
        city: person.city,
        cap: person.cap,
        piva: person.piva,
        phone: person.phone,
        note: person.note,
        email: person.email
      }).toPromise();
    console.debug(response.json().data)
    return response.json().data;
  }

  async updateAnagrafica(person:any): Promise<number> {
    const response = await this.http.put(this.anagraficaURL+"anagrafica/" + person.id, {
        name: person.name,
        address: person.address,
        city: person.city,
        cap: person.cap,
        piva: person.piva,
        phone: person.phone,
        note: person.note,
        email: person.email
      }).toPromise();
    console.debug(response.json().data)
    return response.json().data;
  }

  async deleteAnagrafica(id: Number): Promise<number> {
    const response = await this.http.delete(this.anagraficaURL+"anagrafica/" + id).toPromise();
    console.debug(response.json().data)
    return response.json().data;
  }

}
