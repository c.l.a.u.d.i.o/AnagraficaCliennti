<?php

use Illuminate\Database\Seeder;

class AnagraficasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Anagrafica::class,10)->create();  
    }
}
