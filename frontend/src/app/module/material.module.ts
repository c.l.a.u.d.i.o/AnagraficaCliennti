import { NgModule } from '@angular/core';

import { MatButtonModule, MatListModule, MatCheckboxModule, MatToolbarModule, MatInputModule, MatProgressSpinnerModule, MatCardModule, MatHint, MatIconModule, MatButtonToggleModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';




@NgModule({
  imports: [    
    MatButtonModule,
    MatToolbarModule,
    FormsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatIconModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatOptionModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatListModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatDialogModule,
    MatSnackBarModule
  ],
  exports: [    
    MatButtonModule,
    MatToolbarModule,
    FormsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatIconModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatOptionModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatListModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatDialogModule,
    MatSnackBarModule
  ],
})
export class MaterialModule { }