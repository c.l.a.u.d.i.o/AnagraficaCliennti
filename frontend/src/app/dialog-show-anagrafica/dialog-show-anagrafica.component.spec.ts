import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogShowAnagraficaComponent } from './dialog-show-anagrafica.component';

describe('DialogShowAnagraficaComponent', () => {
  let component: DialogShowAnagraficaComponent;
  let fixture: ComponentFixture<DialogShowAnagraficaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogShowAnagraficaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogShowAnagraficaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
