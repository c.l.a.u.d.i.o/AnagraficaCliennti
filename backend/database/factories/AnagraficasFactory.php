<?php

use Faker\Generator as Faker;

$factory->define(App\Anagrafica::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10),
        'address' => $faker->text(10),
        'city' => $faker->text(10),
        'cap' => $faker->postcode,
        'piva'=> $faker->text(10),
        'phone' => $faker->phoneNumber,
        'note' => $faker->text(40),
        'email' => $faker->email,

    ];
});
