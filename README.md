# **App Anagrafica Clienti**

La Demo dell'app si trova a questo link: [https://storage.googleapis.com/fateltest.appspot.com/index.html](https://storage.googleapis.com/fateltest.appspot.com/index.html)

![](Architecture.png)


## Architettura

La prima grande scelta che ho fatto è stata quella di disaccoppiare il backend dal frontend, questo per avere maggiore flessibilità all’interno dei framework che ho utilizzato. Quindi ho optato per un approccio **RESTful**, l'istanza di php che gira in **Google AppEngine** fornisce delle API per effettuare operazioni CRUD nel **DB MySQL**.

Per il resto ho deciso di caricare il front-end dell’applicazione in uno **object storage** perchè è molto efficiente ma allo stesso tempo economico.

## Framework

Dato il constraint tecnologico (PHP, js) ho optato per i due più conosciuti, utilizzati e documentati framework che conosco: **Laravel** e **Angular** rispettivamente alle ultime versioni.

## Sviluppo

Per il frontend ho optato per utilizzare la libreria Material di Angular la quale offre diversi component pronti all’uso i quali facevano al mio caso.

La [struttura](frontend/src/app) è abbastanza semplice: un service/provider che mi permette di interfacciarmi con le funzionalità del Backend, e due component da inserire nella modale di modifica o mostra cliente. Il resto è tutto nell main component.

Per il backend ho utilizzato le funzionalità che mi mette a disposizione Laravel per creare delle API: [Api](/backend/routes/api.php) e [Http](/backend/app/Http).

## Considerazioni
L'interfaccia è poco dettagliata e si nota che è stata realizzata velocemente. Se avessi avuto più tempo avrei implementato più funzionalità e curato di più la parte di UI. Ad esempio sarebbe necessario implementare la funzionalità di ricerca tra le anagrafiche e la lista non risulta facilmente leggibile. 
I controlli dei form di input venogno fatti solo lato frontend (senza controllare le reg_exp), buona norma è implementare questi controlli anche lato backend ed creare degli errori custom per farne capire la provenienza.
Sarebbe utile avere anche un autenticazione per gli utenti, magari con diversi peremessi (lato backend praticamente pronta, presente nel framework laravel).
Inoltre mancano gli spinner e logiche da per gestire gli errori.