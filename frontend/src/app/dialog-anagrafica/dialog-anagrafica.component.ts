import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AnagraficaService } from '../services/anagrafica.service';

import {FormControl, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-dialog-anagrafica',
  templateUrl: './dialog-anagrafica.component.html',
  styleUrls: ['./dialog-anagrafica.component.css']
})
export class DialogAnagraficaComponent implements OnInit {

  person: any = {
    name: "",
    address: "",
    city: "",
    cap: "",
    piva: "",
    phone: "",
    note: "",
    email: ""
  };
  personId: Number;

  emailFormControl = new FormControl('', [Validators.pattern(EMAIL_REGEX)]);
  requiredVaildator1: FormControl =  new FormControl('', [Validators.required]);
  requiredVaildator2: FormControl =  new FormControl('', [Validators.required]);
  requiredVaildator3: FormControl =  new FormControl('', [Validators.required]);
  requiredVaildator5: FormControl =  new FormControl('', [Validators.required]);

  constructor(
    private AnagraficaService: AnagraficaService,
    public dialogRef: MatDialogRef<DialogAnagraficaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.personId = data.id;
    }


    async update(){
      let result = await this.AnagraficaService.updateAnagrafica(this.person);
      this.onNoClick();
    }

    onNoClick(): void {
      this.dialogRef.close(true);
    }

    close(){
      this.dialogRef.close(false);
    }

    async ngOnInit() {
        this.person = await this.AnagraficaService.getAnagrafica(this.personId);
    }

}
