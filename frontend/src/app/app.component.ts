import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AnagraficaService } from './services/anagrafica.service';

import {FormControl, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAnagraficaComponent } from './dialog-anagrafica/dialog-anagrafica.component';
import { DialogShowAnagraficaComponent } from './dialog-show-anagrafica/dialog-show-anagrafica.component';
import {MatSnackBar} from '@angular/material';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
  title = 'app';
  anagraficas: any;
  person: any = {
    name: "",
    address: "",
    city: "",
    cap: "",
    piva: "",
    phone: "",
    note: "",
    email: ""
  };
  anagrafica:any;
  insertnewcustomers:boolean = false;

  emailFormControl = new FormControl('', [Validators.pattern(EMAIL_REGEX)]);
  requiredVaildator1: FormControl =  new FormControl('', [Validators.required]);
  requiredVaildator2: FormControl =  new FormControl('', [Validators.required]);
  requiredVaildator3: FormControl =  new FormControl('', [Validators.required]);
  requiredVaildator5: FormControl =  new FormControl('', [Validators.required]);

  constructor(private AnagraficaService: AnagraficaService, public dialog: MatDialog, public snackBar: MatSnackBar) {
  }

  async ngOnInit() {
    this.anagraficas = await this.AnagraficaService.getAnagraficas();
  }

  insertnew(){
    this.insertnewcustomers = true;
  }

  close(){
    this.insertnewcustomers = false;
  }

  async createAnagrafica(){
    let result = await this.AnagraficaService.createAnagrafica(this.person);
    this.insertnewcustomers = false;
    if (result){
      console.log("Reload anagrafica");
      this.anagraficas = await this.AnagraficaService.getAnagraficas();
      this.snackBar.open("Cliente Inserito", "", {
        duration: 2000,
      });
  
    }
  }

  async deleteAnagrafica(id: Number){
    let result = await this.AnagraficaService.deleteAnagrafica(id);
    if (result){
      console.log("Reload anagrafica");
      this.anagraficas = await this.AnagraficaService.getAnagraficas();
      this.snackBar.open("Cliente Cancellato", "", {
        duration: 2000,
      });
    }
  }

  editAnagrafica(anagraficaId: Number){

    let dialogRef = this.dialog.open(DialogAnagraficaComponent, {
      width: '80%',
      data: { id : anagraficaId }
    });

  dialogRef.afterClosed().subscribe( async result => {
      console.log('The dialog was closed');
      console.log(this.anagrafica);
      if(result) 
      {
        this.anagraficas = await this.AnagraficaService.getAnagraficas();
        this.snackBar.open("Clinte modificato", "", {
        duration: 2000,
        });
      }
    });
  }

  showAnagrafica(anagraficaId: Number){
    
        let dialogRef = this.dialog.open(DialogShowAnagraficaComponent, {
          width: '80%',
          data: { id : anagraficaId }
        });
    
      dialogRef.afterClosed().subscribe( async result => {
          console.log('The dialog was closed');
        });
      }

}
