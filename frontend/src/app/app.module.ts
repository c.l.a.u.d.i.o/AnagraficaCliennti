import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MaterialModule } from './module/material.module';
import { AnagraficaService } from './services/anagrafica.service';
import { HttpModule } from '@angular/http';
import { DialogAnagraficaComponent } from './dialog-anagrafica/dialog-anagrafica.component';
import { DialogShowAnagraficaComponent } from './dialog-show-anagrafica/dialog-show-anagrafica.component';


@NgModule({
  declarations: [
    AppComponent,
    DialogAnagraficaComponent,
    DialogShowAnagraficaComponent
  ],
  entryComponents: [
    DialogAnagraficaComponent,
    DialogShowAnagraficaComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpModule

  ],
  providers: [AnagraficaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
