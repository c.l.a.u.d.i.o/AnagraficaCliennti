<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//List Anagraficas
Route::get('anagraficas', 'AnagraficaController@index');

//Get Anagrafica
Route::get('anagrafica/{id}', 'AnagraficaController@show');

//Create or Update new Anagrafica
Route::post('anagrafica', 'AnagraficaController@create');

//Update Anagrafica
Route::put('anagrafica/{id}', 'AnagraficaController@update');

//Delete Anagrafica
Route::delete('anagrafica/{id}', 'AnagraficaController@destroy');
